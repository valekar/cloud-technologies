REGISTER '/usr/lib/pig/piggybank.jar';
define CSVLoader org.apache.pig.piggybank.storage.CSVLoader();
data_1  = LOAD '/1.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);


data_2  = LOAD '/2.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);

data_3  = LOAD '/3.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);

data_4  = LOAD '/4.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);


data = union data_1,data_2,data_3,data_4;

column_data = foreach data generate 	Id as id,
					PostTypeId as posttypeid,
					CreationDate as creationdate,
					Score as score,
					ViewCount as viewcount,
					UserId as user_id,
					DisplayName as display_name,
					FLATTEN(TOKENIZE(Body)) as token;

filtered_data = Filter column_data by (id is not null and token is not null and score is not null and viewcount is not null and user_id is not null and (posttypeid == 1 OR posttypeid == 2));

-- take only necessary columns
only_users = foreach filtered_data GENERATE $5,$6,$7;

user_tokens = GROUP only_users BY (user_id,display_name,token); 

user_tokens_counts = FOREACH user_tokens GENERATE FLATTEN(group) as (user_id,display_name,token), COUNT(only_users) as num_user_token_usage;

--user_token_usage_bag: {group: (user_id: long,display_name: chararray),user_tokens_counts: {(user_id: long,display_name: chararray,token: chararray,num_user_token_usage: long)}}
user_token_usage_bag = GROUP user_tokens_counts BY (user_id,display_name);

-- calculate the term frequency 
--user_terms: {user_id: long,display_name: chararray,token: chararray,num_user_token_usage: long,user_questions_size: long}
user_terms = FOREACH user_token_usage_bag GENERATE 
			FLATTEN(group) as (user_id,display_name), 
			FLATTEN(user_tokens_counts.(token,num_user_token_usage)) as (token,num_user_token_usage),
			SUM(user_tokens_counts.num_user_token_usage) as user_questions_size;

--user_term_freq: {user_id: long,display_name: chararray,token: chararray,term_freq: double}
user_term_freq = FOREACH user_terms GENERATE user_id as user_id, display_name as display_name, token as token, ((double)num_user_token_usage/(double)user_questions_size) as term_freq;

--user_term_usage_bag: {group: chararray,user_term_freq: {(user_id: long,display_name: chararray,token: chararray,term_freq: double)}}
user_term_usage_bag = GROUP user_term_freq by token; 

--questions in which the term used by an user appears(like document frequency)
--ques_term_freq: {user_id: long,display_name: chararray,token: chararray,term_freq: double,num_ques_with_token: long}
ques_term_freq = FOREACH user_term_usage_bag GENERATE FLATTEN(user_term_freq) as (user_id,display_name,token,term_freq), COUNT(user_term_freq) as num_ques_with_token; 

-- calculate the tf idf
tf_idf = FOREACH  ques_term_freq {
				idf = LOG((double)195514/(double)num_ques_with_token);
				tfidf = (double)term_freq*idf;					
				
				GENERATE 
					user_id as user_id,
					display_name as display_name,	
					token as token,
					tfidf as tf_idf;				
				};

--dump total_questions;

--describe tf_idf;


store tf_idf into 'stackoverflow.User_TFIDF' using org.apache.hive.hcatalog.pig.HCatStorer();

