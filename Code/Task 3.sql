-- TOP 10 Posts by score
select posts.body,posts.score from posts order by posts.score desc limit 10;

-- TOP 10 DISTINCT Users by Posts Score
select posts.display_name,max(posts.score) as score from posts group by posts.display_name order by score desc limit 10;

-- Get distinct users who used HADOOP in their posts
select 
    count(posts.user_id) as total,
    posts.display_name as display_name,
    posts.body as body 
from posts 
group by display_name,body 
having upper(body) like '%HADOOP%' 
order by total desc; 

-- Get Distinct users count who used Hadoop in their posts
select count(x.total) from 

(select 
    count(posts.user_id) as total,
    posts.display_name as display_name,
    posts.body as body 
from posts 
group by display_name,body 
having upper(body) like '%HADOOP%' 
order by total desc) x; 
