-------------HIVE----------------------------

--Create table in Hive
CREATE TABLE Posts(
    	id bigint,
	posttypeid bigint,
	creationdate varchar(100),
	score bigint,
	viewcount bigint,
	user_id	bigint,
	display_name varchar(250),
	body string
);



--Create table to store TF-IDF scores 
CREATE TABLE USER_TFIDF(
	user_id  bigint,
	display_name  varchar(140),
	token  varchar(140),
	tf_idf  double

);

-- Get TOP 10 tf-idf scores for an user
select * from user_tfidf where user_id = 8015697 order by tf_idf desc 



------------------HIVE----------------------------------




