/*
The queries used in StackoverFlow to retrieve 200000 records
*/
----------------------------------------------------------------
select top 50000 
  P.Id,
  P.PostTypeId, 
  P.CreationDate,
  P.Score,
  P.ViewCount,
  U.Id as UserId,
  U.DisplayName,
  Body 
from Posts P 
left join Users U on U.Id = P.OwnerUserId
order by viewcount desc;

select top 50000 
  P.Id,
  P.PostTypeId, 
  P.CreationDate,
  P.Score,
  P.ViewCount,
  U.Id as UserId,
  U.DisplayName,
  Body 
from Posts P 
left join Users U on U.Id = P.OwnerUserId
where P.ViewCount<72707
order by viewcount desc;

select top 50000 
  P.Id,
  P.PostTypeId, 
  P.CreationDate,
  P.Score,
  P.ViewCount,
  U.Id as UserId,
  U.DisplayName,
  Body 
from Posts P 
left join Users U on U.Id = P.OwnerUserId
where P.ViewCount<42943
order by viewcount desc;


select top 50000 
  P.Id,
  P.PostTypeId, 
  P.CreationDate,
  P.Score,
  P.ViewCount,
  U.Id as UserId,
  U.DisplayName,
  Body 
from Posts P 
left join Users U on U.Id = P.OwnerUserId
where P.ViewCount<30900
order by viewcount desc;
-----------------------------------------------------