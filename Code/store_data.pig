REGISTER '/usr/lib/pig/piggybank.jar';
define CSVLoader org.apache.pig.piggybank.storage.CSVLoader();
data_1  = LOAD '/1.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);


data_2  = LOAD '/2.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);

data_3  = LOAD '/3.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);

data_4  = LOAD '/4.csv' using CSVLoader(',') As 
					(	Id:long,
						PostTypeId:long,
						CreationDate:chararray,
						Score:long,
						ViewCount:long,
						UserId:long,
						DisplayName:chararray,
						Body:chararray
						);


data = union data_1,data_2,data_3,data_4;

column_data = foreach data generate 	Id as id,
					PostTypeId as posttypeid,
					CreationDate as creationdate,
					Score as score,
					ViewCount as viewcount,
					UserId as user_id,
					DisplayName as display_name,
					Body as body;

filtered_data = Filter column_data by (id is not null and body is not null and score is not null and viewcount is not null and user_id is not null and (posttypeid == 1 OR posttypeid == 2));


--store filtered_data into 'output' using PigStorage(',');

store filtered_data into 'stackoverflow.Posts' using org.apache.hive.hcatalog.pig.HCatStorer();

