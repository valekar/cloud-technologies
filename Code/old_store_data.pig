REGISTER '/usr/lib/pig/piggybank.jar';
define CSVLoader org.apache.pig.piggybank.storage.CSVLoader();
data_1  = LOAD '/1.csv' using CSVLoader(',', 'NO_MULTILINE', 'NOCHANGE', 'SKIP_INPUT_HEADER') As 
					(	Id:long,
						PostTypeId:int,
						AcceptedAnswerId:long,
						ParentId:long,
						CreationDate:chararray,
						DeletionDate:chararray,
						Score:long,
						ViewCount:long,
						Body:chararray,
						OwnerUserId:long,
						OwnerDisplayName:chararray,
						LastEditorUserId:long,
						LastEditorDisplayName:chararray,
						LastEditDate:chararray,
						LastActivityDate:chararray,
						Title:chararray,
						Tags:chararray,
						AnswerCount:long,
						CommentCount:long,
						FavoriteCount:long,
						ClosedDate:chararray,
						CommunityOwnedDate:chararray
						);


data = data_1;

column_data = foreach data generate 	Id as id,
							PostTypeId as posttypeid,
							AcceptedAnswerId as acceptedanswerid,
							ParentId as parentid,
							CreationDate as creationdate,
							DeletionDate as deletiondate,
							Score as score,
							ViewCount as viewcount,
							Body as body,
							OwnerUserId as owneruserid,
							OwnerDisplayName as ownerdisplayname,
							LastEditorUserId as lasteditoruserid,
							LastEditorDisplayName as lasteditordisplayname,
							LastEditDate as lasteditdate,
							LastActivityDate as lastactivitydate,
							Title as title,
							Tags as tags,
							AnswerCount as answercount,
							CommentCount as commentcount,
							FavoriteCount as favoritecount,
							ClosedDate as closeddate,
							CommunityOwnedDate as communityowneddate;

filtered_data = Filter column_data by (id is not null and viewcount is not null and score is not null and body is not null and (posttypeid == 1) and owneruserid is not null);


store filtered_data into 'stackoverflow.Posts' using org.apache.hive.hcatalog.pig.HCatStorer();


